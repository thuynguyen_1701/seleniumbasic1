package step2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by nguyenthihongthuy on 9/7/16.
 */
public class SetUp {


    public WebDriver driver;

    public WebDriver openDriver() {
        // declaration and instantiation of objects/variables
        driver = new FirefoxDriver();
        return driver;
    }

    public void closeDriver() {
        // declaration and instantiation of objects/variables
        driver.close(); //close Firefox

        // exit the program explicitly
        System.exit(0);
    }

    public void openPage() {
        String baseUrl = "http://newtours.demoaut.com";
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";

        // launch Firefox and direct it to the Base URL
        driver.get(baseUrl);

        // get the actual value of the title
        actualTitle = driver.getTitle();

        /*
         * compare the actual title of the page witht the expected one and print
         * the result as "Passed" or "Failed"
         */
        if (actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }


    }

}
